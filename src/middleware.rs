use crate::{Account, BankingApp, Transaction};
use restson::{Error, RestPath};
use serde_derive::{Deserialize, Serialize};

// Accounts
// ---------

#[derive(Deserialize, Debug)]
struct PBAccountsDataAccounts {
    accountId: String,
    accountNumber: String,
    accountName: String,
    referenceName: String,
    productName: String,
}

#[derive(Deserialize, Debug)]
struct PBAccountsData {
    accounts: Vec<PBAccountsDataAccounts>,
}

#[derive(Deserialize, Debug)]
struct PBAccounts {
    data: PBAccountsData,
}

impl RestPath<()> for PBAccounts {
    fn get_path(_: ()) -> Result<String, Error> {
        Ok(String::from("za/pb/v1/accounts"))
    }
}

// Transactions
// ------------

#[derive(Deserialize, Debug)]
struct PBTransactionsDataTransactions {
    #[serde(rename = "type")]
    kind: String,
    status: String,
    description: String,
    postedOrder: u128,
    postingDate: String,
    amount: f64,
}

#[derive(Deserialize, Debug)]
struct PBTransactionsData {
    transactions: Vec<PBTransactionsDataTransactions>,
}

#[derive(Deserialize, Debug)]
struct PBTransactions {
    data: PBTransactionsData,
}

impl RestPath<&str> for PBTransactions {
    fn get_path(account_id: &str) -> Result<String, Error> {
        Ok(format!("/za/pb/v1/accounts/{account_id}/transactions"))
    }
}

// Authorisation
// ------------

#[derive(Serialize, Debug)]
struct PBAuth {}

impl RestPath<()> for PBAuth {
    fn get_path(_: ()) -> Result<String, Error> {
        Ok(format!("/identity/v2/oauth2/token"))
    }
}

impl BankingApp {
    fn auth(&mut self) {
        if let Some(client) = self.client.as_mut() {
            client
                .set_header("Accept", "application/json")
                .unwrap_or(());
            client
                .set_header("Content-Type", "application/x-www-form-urlencoded")
                .unwrap_or(());
            client.set_auth(self.client_id.as_str(), self.client_secret.as_str());
            let res = client.post((), &PBAuth {});
            println!("{res:?}");
            // Todo: set the token in the header
        }
    }

    pub fn update_accounts(&mut self) {
        self.auth();
        if let Some(client) = &self.client.as_ref() {
            let res = client.get::<_, PBAccounts>(());
            if let Ok(ok) = res {
                self.accounts = ok
                    .data
                    .accounts
                    .iter()
                    .map(|acc| {
                        (
                            acc.accountId.clone(),
                            Account {
                                number: acc.accountNumber.clone(),
                                name: acc.accountName.clone(),
                                reference: acc.referenceName.clone(),
                                kind: acc.productName.clone(),
                            },
                        )
                    })
                    .collect();
            } else if let Err(err) = res {
                println!("Accounts fetch error: {}", err);
            }
        }
    }

    pub fn update_transactions(&mut self, account_id: &str) {
        self.auth();
        if let Some(client) = &self.client.as_ref() {
            let res = client.get::<_, PBTransactions>(account_id);
            if let Ok(ok) = res {
                self.transactions = ok
                    .data
                    .transactions
                    .iter()
                    .map(|trans| Transaction {
                        status: trans.status.clone(),
                        description: trans.description.clone(),
                        order: trans.postedOrder,
                        date_posted: trans.postingDate.clone(),
                        amount: trans.amount * if trans.kind == "DEBIT" { -1.0 } else { 1.0 },
                    })
                    .collect();
            } else if let Err(err) = res {
                println!("Transactions fetch error: {}", err);
            }
        }
    }
}
