use eframe::egui;
use eframe::egui::{RichText, Ui};
use egui::widgets::global_dark_light_mode_switch;
use egui_extras::{Column, TableBuilder};
use restson::blocking::RestClient;

mod middleware;

use std::collections::{BTreeMap, BinaryHeap};

fn main() {
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "Banking App",
        native_options,
        Box::new(|cc| Box::new(BankingApp::new(cc))),
    );
}

#[derive(Default, Clone)]
enum Page {
    #[default]
    Accounts,
    Transactions(String),
    Settings,
}

#[derive(Clone)]
pub struct Account {
    number: String,
    name: String,
    kind: String,
    reference: String,
}

#[derive(Clone)]
pub struct Transaction {
    status: String,
    description: String,
    order: u128,
    date_posted: String,
    amount: f64,
}

impl Ord for Transaction {
    fn cmp(&self, rhs: &Self) -> std::cmp::Ordering {
        self.order.cmp(&rhs.order)
    }
}

impl Eq for Transaction {}

impl PartialOrd for Transaction {
    fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering> {
        if self.order > rhs.order {
            Some(std::cmp::Ordering::Greater)
        } else if self.order < rhs.order {
            Some(std::cmp::Ordering::Less)
        } else if self.order == rhs.order {
            Some(std::cmp::Ordering::Equal)
        } else {
            None
        }
    }
}

impl PartialEq for Transaction {
    fn eq(&self, rhs: &Self) -> bool {
        self.order == rhs.order
    }
}

#[derive(Default)]
pub struct BankingApp {
    accounts: BTreeMap<String, Account>,
    transactions: BinaryHeap<Transaction>,
    page: Page,
    client: Option<RestClient>,
    refresh_token: String,
    token: String,
    client_id: String,
    client_secret: String,
}

impl BankingApp {
    fn new(_cc: &eframe::CreationContext<'_>) -> Self {
        let mut new = Self::default();

        new.client =
            Some(restson::RestClient::new_blocking("https://openapi.investec.com").unwrap());
        //Some(restson::RestClient::new_blocking("http://localhost").unwrap());
        new
    }
}

impl BankingApp {
    fn go_to(&mut self, page: Page) {
        match &page {
            Page::Accounts => self.update_accounts(),
            Page::Transactions(id) => self.update_transactions(id.as_str()),
            Page::Settings => (),
        }
        self.page = page;
    }

    fn transactions_view(&mut self, ui: &mut Ui) {
        let table = TableBuilder::new(ui)
            .striped(true)
            .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
            .columns(Column::auto().resizable(true), 3)
            .column(Column::remainder())
            .min_scrolled_height(0.0);
        table
            .header(20.0, |mut header| {
                header.col(|ui| {
                    ui.strong("Date Posted");
                });
                header.col(|ui| {
                    ui.strong("Amount");
                });
                header.col(|ui| {
                    ui.strong("status");
                });
                header.col(|ui| {
                    ui.strong("description");
                });
            })
            .body(|mut body| {
                for transaction in self.transactions.clone() {
                    body.row(30.0, |mut row| {
                        row.col(|ui| {
                            ui.label(transaction.date_posted);
                        });
                        row.col(|ui| {
                            ui.label(transaction.amount.to_string());
                        });
                        row.col(|ui| {
                            ui.label(transaction.status);
                        });
                        row.col(|ui| {
                            ui.label(transaction.description);
                        });
                    });
                }
            });
    }

    fn accounts_view(&mut self, ui: &mut Ui) {
        ui.horizontal(|ui| {
            if ui.button("⟳").clicked() {
                self.update_accounts();
            }
        });
        let table = TableBuilder::new(ui)
            .striped(true)
            .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
            .columns(Column::auto().resizable(true), 4)
            .column(Column::remainder())
            .min_scrolled_height(0.0);
        table
            .header(20.0, |mut header| {
                header.col(|_| {});
                header.col(|ui| {
                    ui.strong("Account Name");
                });
                header.col(|ui| {
                    ui.strong("Account Number");
                });
                header.col(|ui| {
                    ui.strong("Type");
                });
                header.col(|ui| {
                    ui.strong("Reference");
                });
            })
            .body(|mut body| {
                for (id, account) in self.accounts.clone() {
                    body.row(30.0, |mut row| {
                        row.col(|ui| {
                            if ui.button("View").clicked() {
                                self.go_to(Page::Transactions(id));
                            }
                        });
                        row.col(|ui| {
                            ui.label(account.name);
                        });
                        row.col(|ui| {
                            ui.label(account.number);
                        });
                        row.col(|ui| {
                            ui.label(account.kind);
                        });
                        row.col(|ui| {
                            ui.label(account.reference);
                        });
                    });
                }
            });
    }

    fn settings_view(&mut self, ui: &mut Ui) {
        ui.horizontal(|ui| {
            ui.heading("Settings");
        });
        ui.horizontal(|ui| {
            ui.label("Client ID: ");
            ui.text_edit_singleline(&mut self.client_id);
            self.client_id = self.client_id.trim().to_string();
        });
        ui.horizontal(|ui| {
            ui.label("Client Secret: ");
            ui.text_edit_singleline(&mut self.client_secret);
            self.client_secret = self.client_secret.trim().to_string();
        });
        ui.horizontal(|ui| {
            ui.label("Toggle Dark Mode: ");
            global_dark_light_mode_switch(ui);
        });
        ui.horizontal(|ui| {
            if ui.button("Ok").clicked() {
                self.go_to(Page::Accounts);
            }
        });
    }
}

impl eframe::App for BankingApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical(|ui| {
                ui.with_layout(egui::Layout::right_to_left(egui::Align::TOP), |ui| {
                    if self.client_id.is_empty() || self.client_secret.is_empty() {
                        ui.strong("Please set your credentials under settings ⬇");
                    }
                });
                ui.with_layout(egui::Layout::right_to_left(egui::Align::TOP), |ui| {
                    if ui.button(RichText::new("⚙").size(20.0)).clicked() {
                        self.go_to(Page::Settings);
                    }
                });

                // Breadcrumb
                ui.horizontal(|ui| {
                    if let Page::Transactions(id) = self.page.clone() {
                        if ui.button("⬅").clicked() {
                            self.go_to(Page::Accounts);
                        }
                        ui.heading(
                            self.accounts
                                .get(id.as_str())
                                .map(|a| a.name.clone())
                                .unwrap_or("".to_string()),
                        );
                    }
                });

                match &self.page {
                    Page::Accounts => self.accounts_view(ui),
                    Page::Transactions(id) => self.transactions_view(ui),
                    Page::Settings => self.settings_view(ui),
                }
            });
        });
    }
}
