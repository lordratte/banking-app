# Banking App

A Work-in-progress app to manage a personal Investect Bank Account.

# Build instructions

## Ensure you have Rust on your computer

For Unix-like computers run this command:

```
~$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Otherwise, have a look at [the website](https://www.rust-lang.org/learn/get-started).

## Run the programme

```
~$ cargo run
```

# Screenshots

![](./screenshot_1.png)

![](./screenshot_2.png)

![](./screenshot_3.png)
